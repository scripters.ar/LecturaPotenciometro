int potenciometro = A0;   // seleccionamos pin para el potenciometro
int val = 0;              // variable para leer estado del potenciometro

void setup() {
  Serial.begin(9600);
  pinMode(potenciometro, INPUT);    // configuramos el potenciometro como Entrada
}

void loop() {
  val = analogRead(potenciometro);
  Serial.println(val);  // Imprimimos el valor del potenciometro por Serial
}

