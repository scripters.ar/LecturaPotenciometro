# LecturaPotenciometro

En este repositorio encontraras información básica sobre como leer y conectar un potenciometro con Arduino a travez de comunicación serial.

## Materiales

* **Una placa Arduino**
* **Una potenciometro**
* **Un Protoboard**

## Uso

```
En la carpeta diagrams encontraras la conexion del circuito.
```

```
En la carpeta src encontraras el codigo fuente de ejemplo.
```

```
Una vez compilado el codigo en el IDE de arduino ve a Herramientas --> Monitor Serial
```

Una vez activado el monitor serial nos mostrara información sobre el potenciometro.

## Autor

* **Edermar Dominguez** - [Ederdoski](https://github.com/ederdoski)

## License

This code is open-sourced software licensed under the [MIT license.](https://opensource.org/licenses/MIT)

